<?php

// src/PlatformBundle/User/ServiceUser.php

namespace PlatformBundle\Services\User;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use PlatformBundle\Entity\User;


class ServiceUser {

    /**
     * 	@var	EntityManagerInterface
     */
    private $em;
    private $requestStack;
    private $request;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $em) {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }
    
    public function rechercheEleve() {
        return $listeEleve = $this->em->getRepository('PlatformBundle:User')->rechercheEleve();
    }
    
    public function rechercheProf() {
        return $listeEleve = $this->em->getRepository('PlatformBundle:User')->rechercheProf();
    }

    public function addUser(User $user) {
        $session = $this->request->getSession();

        if ($user->getProf() === true) {
            $user->setRoles(array('ROLE_PROF'));
        } else {
            $user->setRoles(array('ROLE_ELEVE'));
        }
        $this->em->persist($user);
        $this->em->flush();
        $session->getFlashBag()->add('success', 'Bienvenue ' . $user->getNom() . ' ' . $user->getPrenom());
    }
    
    public function eleveDelete(User $eleve) {
        $session = $this->request->getSession();
        $listecours = $this->em->getRepository('PlatformBundle:User')->userDelete($eleve);
        $session->getFlashBag()->add('success', 'Eleve supprimé.');
    }
    
     public function profDelete(User $prof) {
        $session = $this->request->getSession();
        $listecours = $this->em->getRepository('PlatformBundle:User')->userDelete($prof);
        $session->getFlashBag()->add('success', 'Prof supprimé.');
    }

    public function editUser(User $user) {
        $session = $this->request->getSession();
        $this->em->flush();
        $session->getFlashBag()->add('notice', 'Vos modification ont bien été effectué.');
    }

    public function editMdpUser(User $user) {
        $session = $this->request->getSession();
        $this->em->flush();
        $session->getFlashBag()->add('notice', 'Votre mot de passe a bien été modifié.');
    }

    public function editEmailUser(User $user) {
        $session = $this->request->getSession();
        $this->em->flush();
        $session->getFlashBag()->add('notice', 'Votre email a bien été modifié.');
    }

    public function codepostal(User $user) {
        $user->setCodepostal(intval($user->getCodepostal()));
        if ($user->getCodepostal() === 0) {
            $user->setCodepostal(null);
        }
        return $user;
    }

}
