<?php

// src/PlatformBundle/User/ServiceCours.php

namespace PlatformBundle\Services\Cours;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use PlatformBundle\Entity\Cours;
use PlatformBundle\Entity\User;
use PlatformBundle\Entity\CoursUser;

class ServiceCours {

    /**
     * 	@var	EntityManagerInterface
     */
    private $em;
    private $requestStack;
    private $request;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $em) {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function rechercheCours() {
        return $listecours = $this->em->getRepository('PlatformBundle:Cours')->findAll();
    }

    public function rechercheCoursPratique($pratique) {
        return $listecours = $this->em->getRepository('PlatformBundle:Cours')->findCoursPratique($pratique);
    }

    public function registerCours(User $eleve, Cours $cours) {
        $session = $this->request->getSession();
        $stillregister = false;
        $nbEleve = $this->nbEleve($cours);
        $eleveInscrit = $this->eleveInscrit($cours, $eleve);

        if ($nbEleve >= $cours->getElevemax()) {
            $session->getFlashBag()->add('warning', 'Le nombre d\'élève maximum a été atteint.');
        } else {
            if ($eleveInscrit >= 1) {
                $stillregister = true;
                $session->getFlashBag()->add('info', 'Vous êtes déjà inscrit à ce cours');
            }
            if ($stillregister === false) {
                $coursUser = new CoursUser();
                $coursUser->setCours($cours);
                $coursUser->setUser($eleve);
                $this->em->persist($coursUser);
                $this->em->flush();
                $session->getFlashBag()->add('success', 'Vous êtes bien inscrit à ce cours.');
            }
        }
    }

    public function nbEleve(Cours $cours) {
        return $this->em->getRepository('PlatformBundle:CoursUser')->findNbEleve($cours);
    }

    public function eleveInscrit(Cours $cours, User $eleve) {
        return $this->em->getRepository('PlatformBundle:CoursUser')->findUserCours($cours, $eleve);
    }

    public function rechercheCoursInscrit(User $eleve) {
        return $listecours = $this->em->getRepository('PlatformBundle:CoursUser')->findCoursByUser($eleve);
    }

    public function deleteCoursInscrit(User $eleve, Cours $cours) {
        $session = $this->request->getSession();
        $listecours = $this->em->getRepository('PlatformBundle:CoursUser')->deleteCours($eleve, $cours);
        $session->getFlashBag()->add('success', 'Vous êtes bien désinscrit du cours.');
    }

    public function addCoursRdv(Cours $cours) {
        $session = $this->request->getSession();
        $cours->setPratique(0);
        $this->em->persist($cours);
        $this->em->flush();
        $session->getFlashBag()->add('success', 'Le cours a bien été créé.');
    }

    public function addCoursPratique(Cours $cours) {
        $session = $this->request->getSession();
        $cours->setPratique(1);
        $this->em->persist($cours);
        $this->em->flush();
        $session->getFlashBag()->add('success', 'Le cours a bien été créé.');
    }

    public function rechercheCoursCree(User $prof) {
        return $listecours = $this->em->getRepository('PlatformBundle:Cours')->findCoursByUser($prof);
    }

    public function editCours(Cours $cours) {
        $session = $this->request->getSession();
        $this->em->flush();
        $session->getFlashBag()->add('success', 'Le cours a bien été modifié.');
    }

    public function removeCours(Cours $cours) {
        $session = $this->request->getSession();
        $this->em->getRepository('PlatformBundle:Cours')->removeCours($cours->getId());
        $session->getFlashBag()->add('success', 'Le cours a bien été supprimé.');
    }

    public function eleveCours(Cours $cours) {
        return $this->em->getRepository('PlatformBundle:CoursUser')->findEleveCours($cours);
    }

    public function signalementCours(Cours $cours) {
        $session = $this->request->getSession();
        $cours->setSignalement($cours->getSignalement() + 1);
        $this->em->flush();
        $session->getFlashBag()->add('success', 'Le cours a bien été signalé.');
    }
    
    public function rechercheSignalement() {
        return $this->em->getRepository('PlatformBundle:Cours')->findSignalement();
    }
    
    public function removeSignalement(Cours $cours) {
        $session = $this->request->getSession();
        $this->em->getRepository('PlatformBundle:Cours')->removeCours($cours->getId());
        $session->getFlashBag()->add('success', 'Le cours a bien été supprimé.');
    }
    
    public function zeroSignalement(Cours $cours) {
        $session = $this->request->getSession();
        $this->em->getRepository('PlatformBundle:Cours')->zeroSignalement($cours->getId());
        $session->getFlashBag()->add('success', 'Le cours a bien été remis à zéro.');
    }

}
