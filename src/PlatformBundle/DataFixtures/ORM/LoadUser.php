<?php

// src/PlatformBundle/DataFixtures/ORM/LoadUser.php

namespace PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PlatformBundle\Entity\User;

class LoadUser implements FixtureInterface {

    public function load(ObjectManager $manager) {



        $user = new User;

        $user->setEmail('jean.baptiste.pham77@gmail.com');
        $user->setPlainPassword('test');
        $user->setRoles(array('ROLE_USER'));
        $manager->persist($user);

        $manager->flush();
    }

}
