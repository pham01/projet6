<?php

namespace PlatformBundle\Entity;

use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"email"}, message="Cette adresse mail est déjà utilisée.")
 * @ORM\Entity(repositoryClass="PlatformBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     *
     * @Assert\Email(
     *     message = "Le mail '{{ value }}' n'est pas un mail valide.",
     *     checkMX = true
     * )
     * @Assert\NotBlank(message = "Ce champ ne peut être vide.")
     */
    private $email;

    /**
     * The encoded password
     *
     * @ORM\Column(type="string")
     * 
     */
    private $password;

    /**
     * A non-persisted field that's used to create the encoded password.
     *
     * @var string
     * @Assert\NotBlank(groups={"Registration"})
     * @Assert\Length(
     *      min = 5,
     *      minMessage = "Votre mot de passe doit contenir au moins 5 caractères.",
     * )
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = [];

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre nom ne peut contenir que des lettres."
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre prenom ne peut contenir que des lettres."
     * )
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     * )
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="codepostal", type="string", length=5, nullable=true)
     * @Assert\Length(
     *      min = 5,
     *      max = 5,
     *      minMessage = "Votre code postal doit contenir 5 chiffres.",
     *      maxMessage = "Votre code postal doit contenir 5 chiffres."
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="Vous ne pouvez entrer que des chiffres."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Vous ne pouvez entrer de nombre négatif.",
     * )
     */
    private $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="presentation", type="text", nullable=true)
     */
    private $presentation;

    /**
     * @ORM\OneToOne(targetEntity="PlatformBundle\Entity\Image", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="prof", type="boolean", nullable=false, options={"default":false})
     */
    private $prof;

    public function getUsername() {
        return $this->email;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function setRoles(array $roles) {
        $this->roles = $roles;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
        // leaving blank - I don't need/have a password!
    }

    public function eraseCredentials() {
        $this->plainPassword = null;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                $this->email,
                $this->password
                ) = unserialize($serialized);
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPlainPassword() {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword) {
        $this->plainPassword = $plainPassword;
        // forces the object to look "dirty" to Doctrine. Avoids
        // Doctrine *not* saving this entity, if only plainPassword changes
        $this->password = null;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return User
     */
    public function setVille($ville) {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * Set codepostal.
     *
     * @param string $codepostal
     *
     * @return User
     */
    public function setCodepostal($codepostal) {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get codepostal.
     *
     * @return string
     */
    public function getCodepostal() {
        return $this->codepostal;
    }

    /**
     * Set presentation.
     *
     * @param string $presentation
     *
     * @return User
     */
    public function setPresentation($presentation) {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * Get presentation.
     *
     * @return string
     */
    public function getPresentation() {
        return $this->presentation;
    }

    /**
     * Set image.
     *
     * @param Image|null $image
     *
     * @return User
     */
    public function setImage(Image $image = null) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return Image|null
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set prof.
     *
     * @param bool $prof
     *
     * @return User
     */
    public function setProf($prof) {
        $this->prof = $prof;

        return $this;
    }

    /**
     * Get prof.
     *
     * @return bool
     */
    public function getProf() {
        return $this->prof;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

}
