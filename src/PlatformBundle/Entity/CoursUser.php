<?php
// src/OC/PlatformBundle/Entity/CoursUser.php

namespace PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cours_user")
 * @ORM\Entity(repositoryClass="PlatformBundle\Repository\CoursUserRepository")
 */
class CoursUser
{
  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="PlatformBundle\Entity\Cours")
   * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
   */
  private $cours;

  /**
   * @ORM\ManyToOne(targetEntity="PlatformBundle\Entity\User")
   * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
   */
  private $user;

  /**
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }


    /**
     * Set cours.
     *
     * @param \PlatformBundle\Entity\Cours $cours
     *
     * @return CoursUser
     */
    public function setCours(Cours $cours)
    {
        $this->cours = $cours;

        return $this;
    }

    /**
     * Get cours.
     *
     * @return \PlatformBundle\Entity\Cours
     */
    public function getCours()
    {
        return $this->cours;
    }

    /**
     * Set user.
     *
     * @param \PlatformBundle\Entity\User $user
     *
     * @return CoursUser
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \PlatformBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
