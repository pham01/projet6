<?php

namespace PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use PlatformBundle\Validator\Champobligatoire;
use PlatformBundle\Validator\Dateprise;

/**
 * Cours
 *
 * @ORM\Table(name="cours")
 * @ORM\Entity(repositoryClass="PlatformBundle\Repository\CoursRepository")
 */
class Cours {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datereunion", type="datetime" , nullable=true)
     * @Assert\DateTime()
     * @Champobligatoire()
     */
    private $datereunion;

    /**
     * @var string
     *
     * @ORM\Column(name="adressereunion", type="string", length=255, nullable=true)
     * @Champobligatoire()
     */
    private $adressereunion;

    /**
     * @var string
     *
     * @ORM\Column(name="villereunion", type="string", length=255, nullable=true)
     * @Champobligatoire()
     */
    private $villereunion;

    /**
     * @var int
     *
     * @ORM\Column(name="codepostalreunion", type="integer", nullable=true)
     * @Assert\Length(
     *      min = 5,
     *      max = 5,
     *      minMessage = "Votre code postal doit contenir 5 chiffres.",
     *      maxMessage = "Votre code postal doit contenir 5 chiffres."
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="Vous ne pouvez entrer que des chiffres."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Vous ne pouvez entrer de nombre négatif.",
     * )
     * @Champobligatoire()
     */
    private $codepostalreunion;

    /**
     * @var string
     *
     * @ORM\Column(name="contenue", type="text")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     */
    private $contenue;

    /**
     * @var int
     *
     * @ORM\Column(name="elevemax", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer",
     *     message="Vous ne pouvez entrer que des chiffres."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Vous ne pouvez entrer de nombre négatif.",
     * )
     * @Champobligatoire()
     */
    private $elevemax;

    /**
     * @ORM\ManyToOne(targetEntity="PlatformBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="pratique", type="boolean", nullable=false, options={"default":false})
     */
    private $pratique;

    /**
     * @var int
     *
     * @ORM\Column(name="signalement", type="integer", nullable=true)
     */
    private $signalement = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Cours
     */
    public function setTitre($titre) {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre() {
        return $this->titre;
    }

    /**
     * Set datereunion
     *
     * @param \DateTime $datereunion
     *
     * @return Cours
     */
    public function setDatereunion($datereunion) {
        $this->datereunion = $datereunion;

        return $this;
    }

    /**
     * Get datereunion
     *
     * @return \DateTime
     */
    public function getDatereunion() {
        return $this->datereunion;
    }

    /**
     * Set adressereunion
     *
     * @param string $adressereunion
     *
     * @return Cours
     */
    public function setAdressereunion($adressereunion) {
        $this->adressereunion = $adressereunion;

        return $this;
    }

    /**
     * Get adressereunion
     *
     * @return string
     */
    public function getAdressereunion() {
        return $this->adressereunion;
    }

    /**
     * Set villereunion
     *
     * @param string $villereunion
     *
     * @return Cours
     */
    public function setVillereunion($villereunion) {
        $this->villereunion = $villereunion;

        return $this;
    }

    /**
     * Get villereunion
     *
     * @return string
     */
    public function getVillereunion() {
        return $this->villereunion;
    }

    /**
     * Set codepostalreunion
     *
     * @param integer $codepostalreunion
     *
     * @return Cours
     */
    public function setCodepostalreunion($codepostalreunion) {
        $this->codepostalreunion = $codepostalreunion;

        return $this;
    }

    /**
     * Get codepostalreunion
     *
     * @return int
     */
    public function getCodepostalreunion() {
        return $this->codepostalreunion;
    }

    /**
     * Set contenue
     *
     * @param string $contenue
     *
     * @return Cours
     */
    public function setContenue($contenue) {
        $this->contenue = $contenue;

        return $this;
    }

    /**
     * Get contenue
     *
     * @return string
     */
    public function getContenue() {
        return $this->contenue;
    }

    /**
     * Set elevemax
     *
     * @param integer $elevemax
     *
     * @return Cours
     */
    public function setElevemax($elevemax) {
        $this->elevemax = $elevemax;

        return $this;
    }

    /**
     * Get elevemax
     *
     * @return int
     */
    public function getElevemax() {
        return $this->elevemax;
    }

    /**
     * Set user.
     *
     * @param User $user
     *
     * @return Cours
     */
    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set pratique.
     *
     * @param bool $pratique
     *
     * @return Cours
     */
    public function setPratique($pratique) {
        $this->pratique = $pratique;

        return $this;
    }

    /**
     * Get pratique.
     *
     * @return bool
     */
    public function getPratique() {
        return $this->pratique;
    }



    /**
     * Set signalement.
     *
     * @param int|null $signalement
     *
     * @return Cours
     */
    public function setSignalement($signalement = null)
    {
        $this->signalement = $signalement;

        return $this;
    }

    /**
     * Get signalement.
     *
     * @return int|null
     */
    public function getSignalement()
    {
        return $this->signalement;
    }
}
