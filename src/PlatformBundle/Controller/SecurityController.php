<?php

// src/PlatformBundleBundle/Controller/SecurityController.php;

namespace PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PlatformBundle\Form\LoginForm;

class SecurityController extends Controller {

    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(Request $request) {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PROF') or $this->get('security.authorization_checker')->isGranted('ROLE_ELEVE') or $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('Impossible d\'acceder à cette page.');
        }
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(LoginForm::class, [
            '_username' => $lastUsername,
        ]);

        return $this->render(
                        'PlatformBundle:Security:login.html.twig', array(
                    'form' => $form->createView(),
                    'error' => $error,
                        )
        );
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction() {
        throw new \Exception('this should not be reached!');
    }

}
