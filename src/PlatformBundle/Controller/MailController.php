<?php

namespace PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use PlatformBundle\Form\Type\MailType;
use PlatformBundle\Entity\User;

class MailController extends Controller {

    /**
     * @Security("has_role('ROLE_PROF') or has_role('ROLE_ELEVE') or has_role('ROLE_ADMIN')")
     */
    public function mailAction(Request $request, User $user) {
        $session = $request->getSession();
        $form = $this->createForm(MailType::class);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted()) {
            $message = \Swift_Message::newInstance()
                    ->setSubject('Projet bénévole : ' . $form->getData()['sujet'])
                    ->setFrom(['contact@louvreprojet.fr' => 'Projet bénévole'])
                    ->setTo($user->getEmail())
                    ->setBody(
                    $this->renderView(
                            'Emails/message.html.twig', array('message' => $form->getData()['message'],'sujet' =>$form->getData()['sujet'])
                    ), 'text/html'
                    )
            ;

            $this->get('mailer')->send($message);
            $session->getFlashBag()->add('success', 'Mail envoyé.');
        }

        return $this->render('PlatformBundle:Mail:contact.html.twig', array('form' => $form->createView(), 'id' => $user->getId()));
    }

}
