<?php

namespace PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use PlatformBundle\Entity\Cours;
use PlatformBundle\Entity\Image;
use PlatformBundle\Entity\User;
use PlatformBundle\Form\Type\CoursRdvType;
use PlatformBundle\Form\Type\RechercheCours;
use PlatformBundle\Form\Type\CoursPratiqueType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class FrontController extends Controller {

    public function indexAction(Request $request) {
        return $this->render('PlatformBundle:Front:index.html.twig');
    }

    public function listecoursAction(Request $request) {
        $service = $this->get('platform.cours');

        $listecours = $service->rechercheCours();
        $form = $this->createForm(RechercheCours::class);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted()) {
            $listecours = $service->rechercheCoursPratique($form->getData()['pratique']);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 5
        );

        return $this->render('PlatformBundle:Front:listecours.html.twig', array('pagination' => $pagination, 'form' => $form->createView()));
    }

    public function coursAction(Cours $cours) {
        $service = $this->get('platform.cours');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ELEVE')) {
            $eleveInscrit = $service->eleveInscrit($cours, $this->getUser());
        } else {
            $eleveInscrit = null;
        }
        $nbEleve = $service->nbEleve($cours);
        $listEleve = $service->eleveCours($cours);
        return $this->render('PlatformBundle:Front:cours.html.twig', array(
                    'cours' => $cours,
                    'nbEleve' => $nbEleve,
                    'eleveInscrit' => $eleveInscrit,
                    'listEleve' => $listEleve)
        );
    }

    /**
     * @Security("has_role('ROLE_PROF') or has_role('ROLE_ELEVE') or has_role('ROLE_ADMIN')")
     */
    public function infoAction(Request $request) {
        return $this->render('PlatformBundle:Front:info.html.twig');
    }

    /**
     * @Security("has_role('ROLE_ELEVE')")
     */
    public function registercoursAction(Cours $cours) {
        $service = $this->get('platform.cours');

        $service->registerCours($this->getUser(), $cours);
        $nbEleve = $service->nbEleve($cours);
        $eleveInscrit = $service->eleveInscrit($cours, $this->getUser());
        return $this->render('PlatformBundle:Front:cours.html.twig', array(
                    'cours' => $cours,
                    'nbEleve' => $nbEleve,
                    'eleveInscrit' => $eleveInscrit));
    }

    /**
     * @Security("has_role('ROLE_ELEVE')")
     */
    public function coursinscritAction(Request $request) {
        $service = $this->get('platform.cours');
        $listecours = $service->rechercheCoursInscrit($this->getUser());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 5
        );

        return $this->render('PlatformBundle:Front:coursinscrit.html.twig', array('pagination' => $pagination));
    }

    /**
     * @Security("has_role('ROLE_ELEVE')")
     */
    public function deletecoursAction(Request $request, Cours $cours) {
        $service = $this->get('platform.cours');
        $service->deleteCoursInscrit($this->getUser(), $cours);
        $listecours = $service->rechercheCoursInscrit($this->getUser());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 5
        );

        return $this->render('PlatformBundle:Front:coursinscrit.html.twig', array('pagination' => $pagination));
    }

    /**
     * @Security("has_role('ROLE_PROF')")
     */
    public function coursprofAction(Request $request) {
        return $this->render('PlatformBundle:Front:coursprof.html.twig');
    }

    /**
     * @Security("has_role('ROLE_PROF')")
     */
    public function addcoursrdvAction(Request $request) {
        $service = $this->get('platform.cours');
        $form = $this->createForm(CoursRdvType::class);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $cours = $form->getData();
            $cours->setUser($this->getUser());
            $service->addCoursRdv($cours);

            return $this->redirectToRoute('bp_addcoursrdv');
        }
        return $this->render('PlatformBundle:Front:addcoursrdv.html.twig', [
                    'form' => $form->createView()
        ]);
    }

    /**
     * @Security("has_role('ROLE_PROF')")
     */
    public function addcourspratiqueAction(Request $request) {
        $service = $this->get('platform.cours');
        $form = $this->createForm(CoursPratiqueType::class);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $cours = $form->getData();
            $cours->setUser($this->getUser());
            $service->addCoursPratique($cours);

            return $this->redirectToRoute('bp_addcourspratique');
        }
        return $this->render('PlatformBundle:Front:addcourspratique.html.twig', [
                    'form' => $form->createView()
        ]);
    }

    /**
     * @Security("has_role('ROLE_PROF')")
     */
    public function courscreeAction(Request $request) {
        $service = $this->get('platform.cours');
        $listecours = $service->rechercheCoursCree($this->getUser());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 5
        );

        return $this->render('PlatformBundle:Front:courscree.html.twig', array('pagination' => $pagination));
    }

    /**
     * @Security("has_role('ROLE_PROF')")
     */
    public function editcoursrdvAction(Request $request, Cours $cours) {
        if ($this->getUser() != $cours->getUser()) {
            throw $this->createAccessDeniedException('Impossible d\'acceder à cette page.');
        }
        $service = $this->get('platform.cours');
        $form = $this->createForm(CoursRdvType::class, $cours);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $cours = $form->getData();
            $service->editCours($cours);

            return $this->redirectToRoute('bp_editcoursrdv', array(
                        'id' => $cours->getId()));
        }
        return $this->render('PlatformBundle:Front:editcoursrdv.html.twig', [
                    'form' => $form->createView()
        ]);
    }

    /**
     * @Security("has_role('ROLE_PROF')")
     */
    public function editcourspratiqueAction(Request $request, Cours $cours) {
        if ($this->getUser() != $cours->getUser()) {
            throw $this->createAccessDeniedException('Impossible d\'acceder à cette page.');
        }
        $service = $this->get('platform.cours');
        $form = $this->createForm(CoursPratiqueType::class, $cours);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $cours = $form->getData();
            $service->editCours($cours);

            return $this->redirectToRoute('bp_editcourspratique', array(
                        'id' => $cours->getId()));
        }
        return $this->render('PlatformBundle:Front:editcourspratique.html.twig', [
                    'form' => $form->createView()
        ]);
    }

    /**
     * @Security("has_role('ROLE_PROF')")
     */
    public function removecoursAction(Request $request, Cours $cours) {
        if ($this->getUser() != $cours->getUser()) {
            throw $this->createAccessDeniedException('Impossible d\'acceder à cette page.');
        }
        $service = $this->get('platform.cours');

        $service->removeCours($cours);
        $listecours = $service->rechercheCoursCree($this->getUser());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 5
        );

        return $this->render('PlatformBundle:Front:courscree.html.twig', array('pagination' => $pagination));
    }

}
