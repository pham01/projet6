<?php

namespace PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlatformBundle\Form\User\UserRegistrationForm;
use PlatformBundle\Form\User\UserEditType;
use PlatformBundle\Form\User\UserEditMdpType;
use PlatformBundle\Form\User\UserEditEmailType;
use PlatformBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends Controller {

    /**
     * @Route("/register", name="user_register")
     */
    public function registerAction(Request $request) {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PROF') or $this->get('security.authorization_checker')->isGranted('ROLE_ELEVE')) {
            throw $this->createAccessDeniedException('Impossible d\'acceder à cette page.');
        }
        
        $service = $this->get('platform.user');

        $form = $this->createForm(UserRegistrationForm::class);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();

            $service->addUser($user);

            return $this->get('security.authentication.guard_handler')
                            ->authenticateUserAndHandleSuccess(
                                    $user, $request, $this->get('app.security.login_form_authenticator'), 'main'
            );
        }
        return $this->render('PlatformBundle:User:register.html.twig', [
                    'form' => $form->createView()
        ]);
    }

    /**
     * @Security("has_role('ROLE_PROF') or has_role('ROLE_ELEVE') or has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request) {
        $service = $this->get('platform.user');

        $user = $this->getUser();
        $form = $this->createForm(UserEditType::class, $user);

        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {

            $service->editUser($user);

            return $this->redirectToRoute('bp_info');
        }

        return $this->render('PlatformBundle:User:edit.html.twig', array(
                    'form' => $form->createView(),)
        );
    }

    /**
     * @Security("has_role('ROLE_PROF') or has_role('ROLE_ELEVE') or has_role('ROLE_ADMIN')")
     */
    public function editmdpAction(Request $request) {
        $service = $this->get('platform.user');

        $user = $this->getUser();
        $form = $this->createForm(UserEditMdpType::class, $user);
        $user = $service->codepostal($user);
        
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {

            $service->editMdpUser($user);

            return $this->redirectToRoute('bp_editmdp');
        }
        return $this->render('PlatformBundle:User:editmdp.html.twig', array(
                    'form' => $form->createView(),));
    }
    
    /**
     * @Security("has_role('ROLE_PROF') or has_role('ROLE_ELEVE') or has_role('ROLE_ADMIN')")
     */
    public function editmailAction(Request $request) {
        $service = $this->get('platform.user');

        $user = $this->getUser();
        $form = $this->createForm(UserEditEmailType::class, $user);
        $user = $service->codepostal($user);
        
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {

            $service->editEmailUser($user);

            return $this->redirectToRoute('bp_editmail');
        }
        return $this->render('PlatformBundle:User:editmail.html.twig', array(
                    'form' => $form->createView(),));
    }

}
