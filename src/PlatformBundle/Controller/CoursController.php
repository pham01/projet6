<?php

namespace PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use PlatformBundle\Entity\Cours;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CoursController extends Controller {

    /**
     * @Security("has_role('ROLE_PROF') or has_role('ROLE_ELEVE') or has_role('ROLE_ADMIN')")
     */
    public function signalementAction(Cours $cours) {
        $service = $this->get('platform.cours');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ELEVE')) {
            $eleveInscrit = $service->eleveInscrit($cours, $this->getUser());
        } else {
            $eleveInscrit = null;
        }
        $nbEleve = $service->nbEleve($cours);
        $listEleve = $service->eleveCours($cours);
        $service->signalementCours($cours);
        return $this->render('PlatformBundle:Front:cours.html.twig', array(
                    'cours' => $cours,
                    'nbEleve' => $nbEleve,
                    'eleveInscrit' => $eleveInscrit,
                    'listEleve' => $listEleve)
        );
    }
    
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removecoursAction(Request $request, Cours $cours) {
        $service = $this->get('platform.cours');
        $service->removeSignalement($cours);
        $listecours = $service->rechercheSignalement();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 20
        );

        return $this->render('PlatformBundle:Admin:listesignalement.html.twig', array('pagination' => $pagination));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function zerosignalementAction(Request $request, Cours $cours) {
        $service = $this->get('platform.cours');
        $service->zeroSignalement($cours);
        $listecours = $service->rechercheSignalement();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 20
        );

        return $this->render('PlatformBundle:Admin:listesignalement.html.twig', array('pagination' => $pagination));
    }
   
}
