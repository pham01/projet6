<?php

namespace PlatformBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use PlatformBundle\Entity\User;
use PlatformBundle\Entity\Cours;
use PlatformBundle\Form\User\UserEditType;

/**
 * @Security("is_granted('ROLE_ADMIN')")
 * @Route("/admin")
 */
class GenusAdminController extends Controller {

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function indexAction() {
        return $this->render('PlatformBundle:Admin:index.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function eleveAction(Request $request) {
        $service = $this->get('platform.user');

        $listeEleve = $service->rechercheEleve();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listeEleve, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:eleve.html.twig', array('pagination' => $pagination));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function elevedeleteAction(Request $request, User $eleve) {
        $service = $this->get('platform.user');
        $service->eleveDelete($eleve);
        
        $listeEleve = $service->rechercheEleve();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listeEleve, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:eleve.html.twig', array('pagination' => $pagination));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function eleveeditAction(Request $request, User $eleve) {
        $service = $this->get('platform.user');

        $form = $this->createForm(UserEditType::class, $eleve);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $service->editUser($user);

            return $this->redirectToRoute('admin_eleve_edit', array(
                        'id' => $eleve->getId()));
        }
        return $this->render('PlatformBundle:Admin:eleveedit.html.twig', [
                    'form' => $form->createView()
        ]);
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function elevecoursinscritAction(Request $request, User $eleve) {
        $service = $this->get('platform.cours');
        $listecours = $service->rechercheCoursInscrit($eleve);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:elevecoursinscrit.html.twig', array('pagination' => $pagination, 'eleve' => $eleve));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deletecoursAction(Request $request, Cours $cours, User $eleve) {
        $service = $this->get('platform.cours');
        $service->deleteCoursInscrit($eleve, $cours);
        
        $listecours = $service->rechercheCoursInscrit($eleve);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:elevecoursinscrit.html.twig', array('pagination' => $pagination, 'eleve' => $eleve));
    }
    
    /**
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function profAction(Request $request) {
        $service = $this->get('platform.user');

        $listeProf = $service->rechercheProf();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listeProf, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:prof.html.twig', array('pagination' => $pagination));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function profdeleteAction(Request $request, User $prof) {
        $service = $this->get('platform.user');
        $service->profDelete($prof);
        
        $listeProf = $service->rechercheProf();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listeProf, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:prof.html.twig', array('pagination' => $pagination));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function profeditAction(Request $request, User $prof) {
        $service = $this->get('platform.user');

        $form = $this->createForm(UserEditType::class, $prof);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $service->editUser($user);

            return $this->redirectToRoute('admin_prof_edit', array(
                        'id' => $prof->getId()));
        }
        return $this->render('PlatformBundle:Admin:profedit.html.twig', [
                    'form' => $form->createView()
        ]);
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function profcourscreeAction(Request $request, User $prof) {
        $service = $this->get('platform.cours');
        $listecours = $service->rechercheCoursCree($prof);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:profcourscree.html.twig', array('pagination' => $pagination, 'prof' => $prof));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function removecoursAction(Request $request, Cours $cours, User $prof) {
        $service = $this->get('platform.cours');
        $service->removeCours($cours);
        
        $listecours = $service->rechercheCoursCree($prof);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 10
        );

        return $this->render('PlatformBundle:Admin:profcourscree.html.twig', array('pagination' => $pagination, 'prof' => $prof));
    }
    
    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function signalementAction(Request $request) {
        $service = $this->get('platform.cours');

        $listecours = $service->rechercheSignalement();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listecours, $request->query->getInt('page', 1), 20
        );

        return $this->render('PlatformBundle:Admin:listesignalement.html.twig', array('pagination' => $pagination));
    }

}
