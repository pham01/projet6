<?php

namespace PlatformBundle\Validator;

use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use DateTime;
use DateInterval;
use PlatformBundle\Entity\User;

class DatepriseValidator extends ConstraintValidator {

    private $requestStack;
    private $em;

// Les arguments déclarés dans la définition du service arrivent au constructeur
// On doit les enregistrer dans l'objet pour pouvoir s'en resservir dans la méthode validate()
    public function __construct(RequestStack $requestStack, EntityManagerInterface $em) {
        $this->requestStack = $requestStack;
        $this->em = $em;
    }

    public function validate($value, Constraint $constraint) {
// Pour récupérer l'objet Request tel qu'on le connait, il faut utiliser getCurrentRequest du service request_stack
        $request = $this->requestStack->getCurrentRequest();

        $user = $this->em
                ->getRepository('PlatformBundle:User')
                ->findByEmail($request->getSession()->get('_security.last_username'))
        ;
        if ($request->get('_route') === "bp_addcoursrdv") {
            $nbDateprise = $this->em
                    ->getRepository('PlatformBundle:Cours')
                    ->findDatepriseAdd($user[0], $value)
            ;
        }
        elseif($request->get('_route') === "bp_editcoursrdv"){
            $nbDateprise = $this->em
                    ->getRepository('PlatformBundle:Cours')
                    ->findDatepriseEdit($user[0], $value, $request->get('id'))
            ;
        }

        if ($nbDateprise >= 1) {
// C'est cette ligne qui déclenche l'erreur pour le formulaire, avec en argument le message
            $this->context->addViolation($constraint->message);
        }
    }

}
