<?php

namespace PlatformBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Champobligatoire extends Constraint {

    public $message = "Ce champ est obligatoire";
    
    public function validateBy() {
        return 'bp_addcoursrdv';
    }

}
