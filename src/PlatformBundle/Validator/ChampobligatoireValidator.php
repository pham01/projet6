<?php

namespace PlatformBundle\Validator;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ChampobligatoireValidator extends ConstraintValidator {

    private $requestStack;

// Les arguments déclarés dans la définition du service arrivent au constructeur
// On doit les enregistrer dans l'objet pour pouvoir s'en resservir dans la méthode validate()
    public function __construct(RequestStack $requestStack) {
        $this->requestStack = $requestStack;
    }

    public function validate($value, Constraint $constraint) {
// Pour récupérer l'objet Request tel qu'on le connait, il faut utiliser getCurrentRequest du service request_stack
        $request = $this->requestStack->getCurrentRequest();

        if (($request->get('_route') === "bp_addcoursrdv" && is_null($value)) || ($request->get('_route') === "bp_editcoursrdv" && is_null($value)) ) {
// C'est cette ligne qui déclenche l'erreur pour le formulaire, avec en argument le message
            $this->context->addViolation($constraint->message);
        }
       
    }
}
