<?php

namespace PlatformBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Dateprise extends Constraint {

    public $message = "Vous avez déjà un cours à cette heure ci.";
    
    public function validateBy() {
        return 'bp_addcoursrdv';
    }

}
