<?php

namespace PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MailType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('sujet', TextType::class, array('required' => true))
                ->add('message', TextareaType::class, array('required' => true, 'attr' => array('rows' => '10')))

        ;
    }

    public function getName() {

        return 'mail';
    }

}
