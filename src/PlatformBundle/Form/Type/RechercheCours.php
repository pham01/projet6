<?php

namespace PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RechercheCours extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('pratique', ChoiceType::class, array(
                    'expanded' => false,
                    'multiple' => false,
                    'choices' => array(
                        'Pratique' => true,
                        'RDV' => false,
                    ),
                ))
        ;
    }

    public function getName() {

        return 'recherchecours';
    }

}
