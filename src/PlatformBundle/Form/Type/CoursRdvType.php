<?php

namespace PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use PlatformBundle\Form\Type\ImageType;
use PlatformBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class CoursRdvType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titre', TextType::class)
                ->add('datereunion', DateTimeType::class, [
                    'widget' => 'single_text',
                    'attr' => ['id' => 'datetimepicker'],
                    'html5' => false,
                    'required' => true,
                ])
                ->add('adressereunion', TextType::class, array('required' => true))
                ->add('villereunion', TextType::class, array('required' => true))
                ->add('codepostalreunion', IntegerType::class, array('required' => true))
                ->add('elevemax', IntegerType::class, array('required' => true))
                ->add('contenue', CKEditorType::class, array('required' => true))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => 'PlatformBundle\Entity\Cours'
        ]);
    }

}
