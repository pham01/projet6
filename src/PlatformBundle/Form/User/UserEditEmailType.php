<?php

// src/PlatformBundle/Form/User/UserEditMdpType.php

namespace PlatformBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


use PlatformBundle\Entity\User;

class UserEditEmailType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email', EmailType::class, array('required' => true))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'PlatformBundle\Entity\User',
            'validation_groups' => array('editmail'),
        ));
    }

}
