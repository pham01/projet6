<?php

// src/PlatformBundle/Form/User/UserEditType.php

namespace PlatformBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use PlatformBundle\Form\Type\ImageType;
use PlatformBundle\Entity\User;

class UserEditType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', TextType::class)
                ->add('prenom', TextType::class)
                ->add('adresse', TextType::class, array('required' => false))
                ->add('ville', TextType::class, array('required' => false))
                ->add('codepostal', IntegerType::class, array('required' => false))
                ->add('presentation', TextareaType::class, array('required' => false, 'attr' => array('rows' => '10')))
                ->add('image', ImageType::class, array('required' => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'PlatformBundle\Entity\User',
            'validation_groups' => array('edit'),
        ));
    }

}
