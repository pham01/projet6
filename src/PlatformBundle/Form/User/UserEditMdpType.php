<?php

// src/PlatformBundle/Form/User/UserEditMdpType.php

namespace PlatformBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use PlatformBundle\Entity\User;

class UserEditMdpType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class
                ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'PlatformBundle\Entity\User',
            'validation_groups' => array('editmdp'),
        ));
    }

}
