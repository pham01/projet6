<?php

namespace PlatformBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use PlatformBundle\Form\Type\ImageType;
use PlatformBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UserRegistrationForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('prof', ChoiceType::class, array(
                    'expanded' => true,
                    'multiple' => false,
                    'choices' => array(
                        'Professeur' => true,
                        'Élève' => false,
                    ),
                ))
                ->add('email', EmailType::class)
                ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class
                ])
                ->add('nom', TextType::class)
                ->add('prenom', TextType::class)
                ->add('adresse', TextType::class, array('required' => false))
                ->add('ville', TextType::class, array('required' => false))
                ->add('codepostal', IntegerType::class, array('required' => false))
                ->add('presentation', TextareaType::class, array('required' => false, 'attr' => array('rows' => '10')))
                ->add('image', ImageType::class, array('required' => false))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['Default', 'Registration']
        ]);
    }

}
