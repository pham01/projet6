<?php

namespace Tests\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use PlatformBundle\Entity\Cours;

class FrontControllerTest extends WebTestCase {

    private $client = null;

    public function setUp() {
        $this->client = static::createClient();
    }

    public function testCorrectpage() {
        $this->client->request('GET', '/listecours');

        $this->assertEquals(
                200, 
                $this->client->getResponse()->getStatusCode()
        );
    }
    
    public function testNotCorrectpage() {
        $this->client->request('GET', 'pagenotcorrect');

        $this->assertEquals(
                404, 
                $this->client->getResponse()->getStatusCode()
        );
    }

}
